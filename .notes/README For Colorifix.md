I will take notes in the `.notes` folder (I love [Obsidian](https://obsidian.md/) for my personal notes, which renders in markdown format)

Usually I would create issues and add notes directly on GitLab either in comments on an issue/MR or directly in the MR changes themselves. For the sake of the time limit, I will use this folder here and check it into VCS so you can see my process

Prior to starting, I had the thought that I might use `LiteStar` (`Starlite`), with `neomodel`/`neo4j` but then I decided against this, again due to time limit

I'll use `FastAPI` and I'll use `sqlite3` because I want pipelines working, and I'm not sure how long it would take to set up a runner with `PostgreSQL`