## Part 0

- [ ] Setup project
- [ ] Copy boilerplate
    - [ ] See pipelines pass
- [ ] Docker setup for database
- [ ] Development

## Part 1 - Normalise data

- [ ] Sanitise data
- [ ] Normalise data
- [ ] Decide database schema
- [ ] Setup
    - [ ] poetry
    - [ ] gitlab ci
    - [ ] pre-commit
    - [ ] Alembic
- [ ] Alembic test setup
- [ ] Define models

## Part 2 - Create REST API
- [ ] `FastAPI` main
- [ ] Routers
- [ ] Dependencies
- [ ] Security
- [ ] Endpoints
    - [ ] Company
    - [ ] Permission group
    - [ ] User
    - [ ] Edit user
    - [ ] Get users
        - [ ] pagination

## Bonus
- [ ] `Docker`
- [ ] `PostgreSQL`
- [ ] 